const express = require('express');
const app = express();
const mongoose = require('mongoose');
const db = mongoose.connect('mongodb://127.0.0.1:27017/api');
const User = require('./models/userModel');
const Role = require('./models/roleModel');
let bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//users
app.get('/api/users', (req, res) => {
    User.find(function(err, users){
        if(err) {
            res.send(err);
        }
        res.json(users);
    });
});

app.post('/api/users', (req, res) => {
    var user = new User();

    user.name = req.body.name;
    user.lastname = req.body.lastname;

    user.save(function(err){
        if(err) {
            res.send(err);
        }
        res.status(201);
        res.json(user);
    });
});

app.delete('/api/users', (req, res) => {
    var user = new User();

    user.name = req.body.name;
    user.lastname = req.body.lastname;

    user.save(function(err){
        if(err) {
            res.send(err);
        }
        res.status(201);
        res.json(user);
    });
});

//roles
app.get('/api/roles', (req, res) => {
    Role.find(function(err, roles){
        if(err) {
            res.send(err);
        }
        res.json(roles);
    });
});

app.post('/api/roles', (req, res) => {
    var role = new Role();

    role.name = req.body.name;

    role.save(function(err){
        if(err) {
            res.send(err);
        }
        res.status(201);
        res.json(role);
    });
});

// handle 404
app.use(function(req, res, next){
    res.status(404);
    res.send({ error: 'Not found' });
    return;
});

app.listen(3000, () => console.log('Example app listening on port 3000!'));
